#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.camel.CamelContext;
import org.apache.camel.CamelContextAware;
import org.apache.camel.component.salesforce.SalesforceComponent;
import org.apache.camel.component.salesforce.SalesforceLoginConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration implements CamelContextAware {

	@Autowired
	private AppProperties properties;

	private CamelContext context;

	/**
	 * ActiveMQ EndPoint Component
	 * 
	 * @return
	 */
	@Bean
	public ActiveMQComponent amq() {

		ActiveMQComponent amq = new ActiveMQComponent(context);
		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
		factory.setBrokerURL(properties.getBrokerUrl());
		factory.setUserName(properties.getUserName());
		factory.setPassword(properties.getPassword());
		amq.setConnectionFactory(factory);
		
		return amq;
	}
	
	/**
	 * Camel Salesforce EndPoint
	 * 
	 * @return
	 */
	@Bean(name = "salesforce")
	public SalesforceComponent salesforce() {

		SalesforceLoginConfig config = new SalesforceLoginConfig();
		config.setClientId(properties.getSfdcClientId());
		config.setClientSecret(properties.getSfdcClientSecret());
		config.setLoginUrl(properties.getSfdcLoginUrl());
		config.setUserName(properties.getSfdcUserName());
		config.setPassword(properties.getSfdcPassword());

		SalesforceComponent salesforce = new SalesforceComponent(context);
		salesforce.setLoginConfig(config);
		return salesforce;
	};

	public CamelContext getCamelContext() {
		return context;
	}

	public void setCamelContext(CamelContext context) {
		this.context = context;
	}
}
