package it.pkg;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AppProperties {

	// ActiveMQ Properties
	@Value("${amq.broker.url}")
	private String brokerUrl;

	@Value("${amq.userName}")
	private String userName;

	@Value("${amq.password}")
	private String password;

	@Value("${amq.destination}")
	private String destination;

	// Salesforce details
	@Value("${sfdc.loginUrl}")
	private String sfdcLoginUrl;

	@Value("${sfdc.clientId}")
	private String sfdcClientId;

	@Value("${sfdc.clientSecret}")
	private String sfdcClientSecret;

	@Value("${sfdc.userName}")
	private String sfdcUserName;

	@Value("${sfdc.password}")
	private String sfdcPassword;

	// Route details
	@Value("${salesforce.action}")
	private String salesforceAction;

	public String getBrokerUrl() {
		return brokerUrl;
	}

	public void setBrokerUrl(String brokerUrl) {
		this.brokerUrl = brokerUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getSfdcLoginUrl() {
		return sfdcLoginUrl;
	}

	public void setSfdcLoginUrl(String sfdcLoginUrl) {
		this.sfdcLoginUrl = sfdcLoginUrl;
	}

	public String getSfdcClientId() {
		return sfdcClientId;
	}

	public void setSfdcClientId(String sfdcClientId) {
		this.sfdcClientId = sfdcClientId;
	}

	public String getSfdcClientSecret() {
		return sfdcClientSecret;
	}

	public void setSfdcClientSecret(String sfdcClientSecret) {
		this.sfdcClientSecret = sfdcClientSecret;
	}

	public String getSfdcUserName() {
		return sfdcUserName;
	}

	public void setSfdcUserName(String sfdcUserName) {
		this.sfdcUserName = sfdcUserName;
	}

	public String getSfdcPassword() {
		return sfdcPassword;
	}

	public void setSfdcPassword(String sfdcPassword) {
		this.sfdcPassword = sfdcPassword;
	}

	public String getSalesforceAction() {
		return salesforceAction;
	}

	public void setSalesforceAction(String salesforceAction) {
		this.salesforceAction = salesforceAction;
	}

}
