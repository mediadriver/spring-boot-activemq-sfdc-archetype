package it.pkg.camel;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.pkg.AppProperties;

@Component
public class RouteBuilderOne extends RouteBuilder {

	@Autowired
	private AppProperties properties;

	
	@Override
	public void configure() throws Exception {
		
		onException(Exception.class).handled(true).stop();
		
		from("amq:queue:"+properties.getDestination()).
		log("LOG RECEIVED MESSAGE").
		to("salesforce:"+properties.getSalesforceAction()).
		log("LOG SENT MESSAGE").
		end();
		
	}

}
